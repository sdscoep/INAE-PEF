package org.coep.sds.conferenceregistration;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class About_PEF_Fragment extends Fragment {

    TextView about;
    public About_PEF_Fragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_inae_about, container, false);
        about = (TextView) v.findViewById(R.id.fragment_inae_about_text);
        Spanned aboutText = Html.fromHtml("<h2>Pune Engineering Forum of-</h2>\n" +
                "<h4>Indian National Academy of Engineering (INAE)</h4>"+"PEF is the Pune chapter of Indian National Academy of Engineering, INAE which was founded in 1987, comprises India’s most distinguished engineers and scientists from Industry, Academia & Research Institutes. Visit site https://inae.in for details.\n" +
                "<br />The Pune Engineering Forum was inaugurated on 27th July 2018 at a function in COEP, by Padma Bhushan Dr B N Suresh, President INAE.\n" +
                "<br />Pune city is a hub for engineering with the brick & mortar  industries ,dominated by Auto sector and the now booming IT sector. The Engineering colleges have kept pace with this trend. The oldest COEP is the pride of India, and many new & modern colleges are contributing. There is valuable presence of many Research institutes.\n" +
                "<br />Keeping this large Engineering community in mind, INAE felt it necessary to open a Pune chapter of the academy. Now named PEF -Pune Engineering Forum.\n" +
                "Some of the aims of PEF will be ..\n" +
                "<ul><li>To encourage & promote the pursuit of excellence in the field of ‘Engineering’</li>\n" +
                "<li>Hold seminars & talks by eminent engineers , in relevant fields\n</li>" +
                "<li>Promote ‘Innovation’ and any steps to  encourage ‘ Start-ups’\n</li>" +
                "<li>Facilitate Industry, academia interactions\n</li>" +
                "<li>Help the MSMEs in technical areas with expertise available from INAE fellows.\n</li>" +
                "<li>Help industry in areas of Safety, Quality & Efficiency improvements.\n</li>" +
                "<li>Steps in making the engineering students ready for Industry.\n</li></ul>" +
                "<b>The forum is open for suggestions from registered members</b> to make it more Vibrant & useful for the purpose of serving national interest thru’ Engineering.");
        about.setText(aboutText);
        return v;
    }
}
