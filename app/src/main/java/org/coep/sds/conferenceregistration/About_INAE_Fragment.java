package org.coep.sds.conferenceregistration;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class About_INAE_Fragment extends Fragment {

    TextView about;
    public About_INAE_Fragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_inae_about, container, false);
        about = (TextView) v.findViewById(R.id.fragment_inae_about_text);
        Spanned aboutText = Html.fromHtml("<h2>Indian National Academy of Engineering (INAE)</h2>"+
                "INAE founded in 1987, comprises India’s most distinguished engineers and scientists from Industry, Academia & Research Institutes. Visit site https://inae.in for details.\n" +
                "<br />It covers the entire spectrum of engineering disciplines. It serves as a Think Tank which has made valuable recommendations to Government from time to time.\n" +
                "<br />INAE is partly funded by Department of Science & Technology, Govt. of India.  It has around 800 Fellows, who are most distinguished persons with a lot of experience & contribution to Indian Engineering area.\n" +
                "The  main aims & objects  of INAE at present are ..\n" +
                "<ul><li>To encourage & promote the pursuit of excellence in the field of ‘Engineering’</li>\n" +
                "<li>To offer Government of India &  local authorities the views in all matters pertaining to ‘Engineering’</li>\n" +
                "<li>To promote the National Policy on education of Govt. of India</li>\n" +
                "<li>To encourage Inventions, investigations and research. Also promote their applications for development of both organised and un-organised sectors of Indian economy.</li>\n" +
                "<li>To institute and establish Professorships, Fellowships, Studentships, Scholarships, awards etc.</li>\n" +
                "<li>INAE represents India at the International Council of Academies of Engineering & Technological Sciences (CAETS)</li></ul>");
        about.setText(aboutText);
        return v;
    }
}
