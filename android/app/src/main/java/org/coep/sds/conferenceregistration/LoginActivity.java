package org.coep.sds.conferenceregistration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    String serverUrl;
    private OkHttpClient okHttpClient;
    private SharedPreferences sharedPreferences;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextInputLayout lNameLayoutPassword;
    private TextInputLayout lNameLayoutEmail;

    public static void backgroundThreadLongToast(final Context context,
                                                 final String msg) {
        if (context != null && msg != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        serverUrl = getResources().getString(R.string.server_root);
        sharedPreferences = getSharedPreferences("App", MODE_PRIVATE);
        okHttpClient = new OkHttpClient();

        /*if (sharedPreferences.getBoolean("isLoggedIn", false)) {
            Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
            startActivity(intent);
            finish();
        }*/
        TextView goToSignUp = findViewById(R.id.go_to_signUp);
        goToSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_NULL) {
                    mPasswordView.requestFocus();
                    return true;
                }
                return false;
            }
        });

        lNameLayoutPassword = (TextInputLayout) findViewById(R.id.TextLayoutPassword);
        lNameLayoutEmail = (TextInputLayout) findViewById(R.id.TextLayoutEmail);

        //lNameLayoutPassword.setErrorEnabled(true);
        //lNameLayoutPassword.setError("Lol, just checking");

        mPasswordView = (EditText) findViewById(R.id.password);

    }

    public void onClickLogin(View view) {

        mEmailView.setError(null);
        mPasswordView.setError(null);
        lNameLayoutEmail.setError(null);
        lNameLayoutPassword.setError(null);

        // Store values at the time of the login attempt.
        final String username = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            lNameLayoutPassword.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            lNameLayoutEmail.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            final MaterialDialog progressDialog = new MaterialDialog.Builder(this)
                    .title("Logging In")
                    .content("Please wait...")
                    .progress(true, 0)
                    .build();
            progressDialog.show();
            RequestBody loginBody = new FormBody.Builder()
                    .add("username", username)
                    .add("password", password)
                    .build();
            post(serverUrl + "login.php", loginBody, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    progressDialog.dismiss();
                    backgroundThreadLongToast(LoginActivity.this, "No internet connection, retry later.");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    switch (response.body().string()) {
                        case "SUCCESS":
                            progressDialog.dismiss();
                            sharedPreferences.edit().putBoolean("isLoggedIn", true).apply();
                            sharedPreferences.edit().putString("username", username);
                            Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("login", "SUCCESS");
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                            break;
                        case "ADMIN":
                            progressDialog.dismiss();
                            Intent adminIntent = new Intent(LoginActivity.this, DrawerActivity.class);
                            Bundle bundle1 = new Bundle();
                            bundle1.putString("login", "ADMIN");
                            adminIntent.putExtras(bundle1);
                            startActivity(adminIntent);
                            finish();
                            break;
                        case "FAILURE":
                            progressDialog.dismiss();
                            backgroundThreadLongToast(LoginActivity.this, "Login Failed");
                            break;
                        default:
                            progressDialog.dismiss();
                            backgroundThreadLongToast(LoginActivity.this, "Something went wrong");
                            break;
                    }
                }
            });
        }

    }

    private boolean isPasswordValid(String password) {
        return password.length() > 7;
    }

    Call post(String url, RequestBody form, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
        return call;
    }
}
