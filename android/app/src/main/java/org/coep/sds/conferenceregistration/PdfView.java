package org.coep.sds.conferenceregistration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

import static org.coep.sds.conferenceregistration.LoginActivity.backgroundThreadLongToast;

public class PdfView extends AppCompatActivity {

    private OkHttpClient okHttpClient;
    private String serverUrl;
    private SharedPreferences sharedPreferences;
    private int latestVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);

        serverUrl = getResources().getString(R.string.server_root);
        okHttpClient = new OkHttpClient();
        sharedPreferences = getSharedPreferences("App", MODE_PRIVATE);

        downloadPdf();

    }

    private void downloadPdf() {
        final File pdfFile = new File(getFilesDir(), "pdf");

        Request request = new Request.Builder()
                .url(serverUrl + "/getVersion.php")
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                backgroundThreadLongToast(PdfView.this, "No internet connection, loading offline data");
                if (pdfFile.exists())
                    loadPdf(pdfFile);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                latestVersion = Integer.parseInt(response.body().string());
                if (sharedPreferences.getInt("pdfVersion", -1) < latestVersion) {
                    try {
                        download(serverUrl + "/PDF/PEF.pdf", pdfFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else
                    loadPdf(pdfFile);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                sharedPreferences.edit().putBoolean("isLoggedIn", false).apply();
                Intent intent = new Intent(PdfView.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.refresh:
                downloadPdf();
                break;
        }
        return true;
    }

    private void loadPdf(File pdfFile) {
        PDFView pdfView = findViewById(R.id.pdfView);
        pdfView.fromFile(pdfFile)
                .enableSwipe(true)
                .enableDoubletap(true)
                .load();
    }

    private void download(@NonNull String url, @NonNull final File destFile) throws IOException {
        Request request = new Request.Builder().url(url).build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                backgroundThreadLongToast(PdfView.this, "No internet connection, loading offline data");
                loadPdf(destFile);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody body = response.body();
                long contentLength = body.contentLength();
                BufferedSource source = body.source();

                BufferedSink sink = Okio.buffer(Okio.sink(destFile));
                Buffer sinkBuffer = sink.buffer();

                long totalBytesRead = 0;
                int bufferSize = 8 * 1024;
                for (long bytesRead; (bytesRead = source.read(sinkBuffer, bufferSize)) != -1; ) {
                    sink.emit();
                    totalBytesRead += bytesRead;
                    int progress = (int) ((totalBytesRead * 100) / contentLength);

                }
                sink.flush();
                sink.close();
                source.close();

                sharedPreferences.edit().putInt("pdfVersion", latestVersion).apply();
                loadPdf(destFile);
            }
        });

    }
}
