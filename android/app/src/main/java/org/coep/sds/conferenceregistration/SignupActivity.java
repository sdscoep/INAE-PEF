package org.coep.sds.conferenceregistration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static org.coep.sds.conferenceregistration.LoginActivity.backgroundThreadLongToast;

public class SignupActivity extends AppCompatActivity {

    String serverUrl;
    private EditText editText_name;
    private EditText editText_email;
    private EditText editText_mobile;
    private EditText editText_password;
    private EditText editText_institute;
    private EditText editText_repeat_password;
    private EditText editText_username;
    private OkHttpClient okHttpClient;
    private TextInputLayout inputLayoutName;
    private TextInputLayout inputLayoutInstitue;
    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPhone;
    private TextInputLayout inputLayoutPassword;
    private TextInputLayout inputLayoutRepeatPassword;
    private TextInputLayout inputLayoutUsername;
    private SharedPreferences sharedPreferences;

    private boolean validity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        serverUrl = getResources().getString(R.string.server_root);
        okHttpClient = new OkHttpClient();
        sharedPreferences = getSharedPreferences("App", MODE_PRIVATE);

        inputLayoutEmail = (TextInputLayout) findViewById(R.id.TextLayoutEmailSignUp);
        inputLayoutInstitue = (TextInputLayout) findViewById(R.id.TextLayoutInstitute);
        inputLayoutName = (TextInputLayout) findViewById(R.id.TextLayoutName);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.TextLayoutPasswordSignUp);
        inputLayoutRepeatPassword = (TextInputLayout) findViewById(R.id.TextLayoutRepeatPassword);
        inputLayoutPhone = (TextInputLayout) findViewById(R.id.TextLayoutPhone);


        editText_username = findViewById(R.id.editText_username);
        editText_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputLayoutName.getError() != null) {
                    verify_name();
                }
            }
        });
        editText_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    verify_name();
                }
            }
        });

        editText_name = findViewById(R.id.editText_name);
        editText_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputLayoutName.getError() != null) {
                    verify_name();
                }
            }
        });
        editText_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    verify_name();
                }
            }
        });

        editText_email = (EditText) findViewById(R.id.editText_email);
        editText_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputLayoutEmail.getError() != null) {
                    verify_email();
                }
            }
        });
        editText_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    verify_email();
                }
            }
        });

        editText_mobile = (EditText) findViewById(R.id.editText_mobile);
        editText_mobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    verify_mobile();
                }
            }
        });
        editText_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputLayoutPhone.getError() != null) {
                    verify_mobile();
                }
            }
        });


        editText_password = findViewById(R.id.editText_password);
        editText_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    verify_password();
                }
            }
        });
        editText_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                verify_password();
            }
        });

        editText_institute = (EditText) findViewById(R.id.editText_institue);
        editText_institute.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    verify_institute();
                }
            }
        });
        editText_institute.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputLayoutInstitue.getError() != null) {
                    verify_institute();
                }
            }
        });

        editText_repeat_password = (EditText) findViewById(R.id.editText_repeat_password);
        editText_repeat_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    verify_repeatPassword();
                }
            }
        });
        editText_repeat_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputLayoutRepeatPassword.getError() != null) {
                    verify_repeatPassword();
                }
            }
        });
        editText_repeat_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == R.id.sign_up_button || actionId == EditorInfo.IME_NULL) {
                    onclickSignUp();
                    return true;
                }
                return false;
            }
        });

        Button signUp = (Button) findViewById(R.id.sign_up_button);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclickSignUp();
            }
        });
    }

    public void onclickSignUp() {
        //To Hide Keyboard
        ((InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        //Checking Credentials are correct
        validity = true;
        verifyAll();


        if (validity) {
            final MaterialDialog progressDialog = new MaterialDialog.Builder(this)
                    .title("Logging In")
                    .content("Please wait...")
                    .progress(true, 0)
                    .build();
            progressDialog.show();
            final String username = editText_username.getText().toString();
            RequestBody loginBody = new FormBody.Builder()
                    .add("username", username)
                    .add("name", editText_name.getText().toString())
                    .add("affiliated", editText_institute.getText().toString())
                    .add("email", editText_email.getText().toString())
                    .add("phone", editText_mobile.getText().toString())
                    .add("password", editText_password.getText().toString())
                    .build();
            post(serverUrl + "signup.php", loginBody, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    backgroundThreadLongToast(SignupActivity.this, "No internet connection, please retry");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    switch (response.body().string()) {
                        case "SUCCESS":
                            progressDialog.dismiss();
                            sharedPreferences.edit().putBoolean("isLoggedIn", true).apply();
                            sharedPreferences.edit().putString("username", username);
                            Intent intent = new Intent(SignupActivity.this, PdfView.class);
                            startActivity(intent);
                            finish();
                            break;
                        case "USERNAME_EXISTS":
                            progressDialog.dismiss();
                            backgroundThreadLongToast(SignupActivity.this, "This username already exists");
                        case "FAILURE":
                            progressDialog.dismiss();
                            backgroundThreadLongToast(SignupActivity.this, "Something went wrong. Please try again");
                    }
                }
            });
        } else {
            backgroundThreadLongToast(SignupActivity.this, "Check Entered credentials");
        }
    }


    Call post(String url, RequestBody form, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
        return call;
    }

    private void verifyAll() {
        verify_name();
        verify_password();
        verify_email();
        verify_mobile();
        verify_repeatPassword();
        verify_institute();
    }

    private void verify_email() {
        String email = editText_email.getText().toString();
        if (!(Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
            inputLayoutEmail.setErrorEnabled(true);
            inputLayoutEmail.setError("Enter a valid email address");
            validity = false;
        } else {
            inputLayoutEmail.setError(null);
            inputLayoutEmail.setErrorEnabled(false);
        }

        Pattern pattern = Pattern.compile("^[a-z0-9.+]*[@][c][o][e][p][.][a][c][.][i][n]$");
        if (pattern.matcher(email).matches()) {
            inputLayoutEmail.setErrorEnabled(true);
            inputLayoutEmail.setError("COEP ID not supported.");
            validity = false;

        }
    }

    private void verify_name() {
        //validity=true;
        String name = editText_name.getText().toString();
        if (name.isEmpty()) {
            inputLayoutName.setErrorEnabled(true);
            inputLayoutName.setError("Please enter your Name");
            validity = false;
        } else {
            inputLayoutName.setError(null);
            inputLayoutName.setErrorEnabled(false);
        }
    }


    private void verify_institute() {
        String institute = editText_institute.getText().toString();
        if (institute.isEmpty()) {
            inputLayoutName.setErrorEnabled(true);
            inputLayoutName.setError("Please enter your Name");
            validity = false;
        } else {
            inputLayoutName.setError(null);
            inputLayoutName.setErrorEnabled(false);
        }
    }

    private void verify_password() {
        String password = editText_password.getText().toString();
        if (password.length() < 8) {
            inputLayoutPassword.setErrorEnabled(true);
            inputLayoutPassword.setError("Password has to be at least 8 characters long.");
            validity = false;
        } else {
            inputLayoutPassword.setError(null);
            inputLayoutPassword.setErrorEnabled(false);
        }
    }

    private void verify_repeatPassword() {
        //validity=true;
        String password = editText_password.getText().toString();
        String repeatPassword = editText_repeat_password.getText().toString();
        if (!(password.equals(repeatPassword))) {
            inputLayoutRepeatPassword.setErrorEnabled(true);
            inputLayoutRepeatPassword.setError("Passwords do not match!!");
            //editText_password.setError("Passwords do not match!!");
            validity = false;
        } else {
            inputLayoutRepeatPassword.setError(null);
            inputLayoutRepeatPassword.setErrorEnabled(false);
        }
    }

    private void verify_mobile() {
        //validity = true;
        Pattern pattern = Pattern.compile("^[789]\\d{9}$");
        String mobile = editText_mobile.getText().toString();
        if (!(pattern.matcher(mobile).matches())) {
            inputLayoutPhone.setErrorEnabled(true);
            inputLayoutPhone.setError("Enter a valid mobile number");
            validity = false;
        } else {
            inputLayoutPhone.setError(null);
            inputLayoutPhone.setErrorEnabled(false);
        }
    }

}
