<?php
require 'db.php';
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $result = $mysqli->query("SELECT * FROM pdfVersion") or die($mysqli->error);
  if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    echo $row['version'];
  }
  else {
    echo "FAILURE";
  }
}
?>
