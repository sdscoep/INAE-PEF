<?php
require 'db.php';
$upload_path = './PDF/';
$response = "Error!";
if($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['username'] == 'admin' && $_POST['password'] == 'admin'){
    if(isset($_FILES['pdf']['name'])){
        $fileinfo = pathinfo($_FILES['pdf']['name']);
        $extension = $fileinfo['extension'];
        $file_path = $upload_path.'PEF.'.$extension;
        if (!file_exists($upload_path)) {
        	$oldumask = umask(0);
          mkdir($upload_path, 0777, true);
        	umask($oldumask);
        }
        try{
            move_uploaded_file($_FILES['pdf']['tmp_name'],$file_path);
            $mysqli->query("UPDATE pdfVersion SET version = version + 1") or die($mysqli->error);
            $response = "Saved!";
        }catch(Exception $e){
            $response = "Failed!";
        }
    }else{
        $response = 'Please choose a file';
    }
    echo $response;
}
?>
