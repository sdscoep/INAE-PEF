package org.coep.sds.conferenceregistration;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

import static org.coep.sds.conferenceregistration.LoginActivity.backgroundThreadLongToast;

public class AdminActivity extends AppCompatActivity {

    public int PDF_REQ_CODE = 1;
    Button downloadButton, UploadButton;
    EditText PdfNameEditText;
    Uri uri;
    String serverUrl;
    String PdfPathHolder, PdfID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        serverUrl = getResources().getString(R.string.server_root);

        AllowRunTimePermission();


        downloadButton = findViewById(R.id.download_button);
        UploadButton = (Button) findViewById(R.id.upload_button);

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                File dataFile = new File(downloadDir, "PEF.csv");
                try {
                    dataFile.createNewFile();
                    download(serverUrl + "downloadCSV.php", dataFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        UploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // PDF selection code start from here .

                Intent intent = new Intent();

                intent.setType("application/pdf");

                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE);

            }
        });

    }

    private void download(@NonNull String url, @NonNull final File destFile) throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody loginBody = new FormBody.Builder()
                .add("username", "admin")
                .add("password", "admin")
                .build();
        Request request = new Request.Builder()
                .post(loginBody)
                .url(url)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                backgroundThreadLongToast(AdminActivity.this, "No internet connection,download failed");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody body = response.body();
                BufferedSource source = body.source();

                BufferedSink sink = Okio.buffer(Okio.sink(destFile));
                Buffer sinkBuffer = sink.buffer();

                long totalBytesRead = 0;
                int bufferSize = 8 * 1024;
                for (long bytesRead; (bytesRead = source.read(sinkBuffer, bufferSize)) != -1; ) {
                    sink.emit();
                    totalBytesRead += bytesRead;
                }
                sink.flush();
                sink.close();
                source.close();
                backgroundThreadLongToast(AdminActivity.this, "Download complete");
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PDF_REQ_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            uri = data.getData();

            PdfUploadFunction();

        }
    }

    public void PdfUploadFunction() {


        PdfPathHolder = FilePath.getPath(this, uri);
        if (PdfPathHolder == null) {
            Toast.makeText(this, "Please move your PDF file to internal storage & try again.", Toast.LENGTH_LONG).show();
        } else {

            try {

                PdfID = UUID.randomUUID().toString();

                new MultipartUploadRequest(this, PdfID, serverUrl + "uploadPDF.php")
                        .addFileToUpload(PdfPathHolder, "pdf")
                        .addParameter("username", "admin")
                        .addParameter("password", "admin")
                        .setMaxRetries(5)
                        .startUpload();

            } catch (Exception exception) {

                Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void AllowRunTimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(AdminActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Toast.makeText(AdminActivity.this, "READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(AdminActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {

        switch (RC) {

            case 1:

                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(AdminActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(AdminActivity.this, "Permission Canceled", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }


}