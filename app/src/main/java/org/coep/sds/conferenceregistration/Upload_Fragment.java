package org.coep.sds.conferenceregistration;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class Upload_Fragment extends Fragment {
    public int PDF_REQ_CODE = 1;
    Uri uri;
    String PdfPathHolder, PdfID;
    String serverUrl;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.upload_fragment, container, false);
        return v;
    }

    @Override
    public void onDestroyView() {
        ViewGroup con = (ViewGroup) getActivity().findViewById(R.id.frame);
        con.removeAllViews();
        super.onDestroyView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serverUrl = getResources().getString(R.string.server_root);
        Intent intent = new Intent();

        intent.setType("application/pdf");

        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PDF_REQ_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            uri = data.getData();

            PdfUploadFunction();

        }
    }
    public void PdfUploadFunction() {


        PdfPathHolder = FilePath.getPath(getContext(), uri);
        if (PdfPathHolder == null) {
            Toast.makeText(getContext(), "Please move your PDF file to internal storage & try again.", Toast.LENGTH_LONG).show();
        } else {

            try {

                PdfID = UUID.randomUUID().toString();

                new MultipartUploadRequest(getContext(), PdfID, serverUrl + "uploadPDF.php")
                        .addFileToUpload(PdfPathHolder, "pdf")
                        .addParameter("username", "admin")
                        .addParameter("password", "admin")
                        .setMaxRetries(5)
                        .startUpload();

            } catch (Exception exception) {

                Toast.makeText(getContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
