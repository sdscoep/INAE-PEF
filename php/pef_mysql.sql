-- MySQL dump 10.16  Distrib 10.2.17-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: u686602787_pef
-- ------------------------------------------------------
-- Server version	10.2.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pdfVersion`
--

DROP TABLE IF EXISTS `pdfVersion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfVersion` (
  `version` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfVersion`
--

/*!40000 ALTER TABLE `pdfVersion` DISABLE KEYS */;
INSERT INTO `pdfVersion` VALUES (8);
/*!40000 ALTER TABLE `pdfVersion` ENABLE KEYS */;

--
-- Table structure for table `userData`
--

DROP TABLE IF EXISTS `userData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userData` (
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `affiliated` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userData`
--

/*!40000 ALTER TABLE `userData` DISABLE KEYS */;
INSERT INTO `userData` VALUES ('a','3dbe00a167653a1aaee01d93e77e730e','Abhishek Jadhav','abhishekjadhav205@gmail.com','9999999999','Coep'),('ameyaapte1','f8aecd56598ad2140efdc974fded72cb','Ameya Apte','ameyaapte1@gmail.com','7588286543','COEP'),('ameyaapte2','f8aecd56598ad2140efdc974fded72cb','Ameya Apte','ameyaapte1@gmail.com','7588286543','COEP'),('ameyaapte3','f8aecd56598ad2140efdc974fded72cb','Test User','ameyaapte1@gmail.com','7588286543','COEP'),('batman','29421e3dbf61f1ed93f2f8594131f6b6','Abhishek Jadhav','batman@cave.com','9999999999','COEP'),('jarvis','f8aecd56598ad2140efdc974fded72cb','Ameya Apte','ameyaapte1@gmail.com','7588286543','COEP'),('k_kaushik','a8bd6de7a52589da78003e8699ca788c','Kaushik Sanjay Kulkarni','kskulkarni100@gmail.com','7588613362','College Of Engineering Pune'),('M V Kotwal','b202e2ec1b8dccb09e4e887477c74004','Madhukar V Kotwal','mvkotwal@gmail.com','9867499199','INAE'),('Manali','519b69b5c17c134e4ea2a8be80639776','Manali Pawar','manalipawar511@gmail.com','8087547973','COEP'),('mvkotwal@gmail.com','92ecb81fc99783cc10c0c509fa0fd10b','M V Kotwal','mvkotwal@gmail.com','9867499199','INAE'),('ramesh.marathe','7bf3d61d4aee453ca1cb573959214edb','Ramesh D Marathe','ramesh.marathe@gmail.com','9821807535','SBM POLYTECHNIC, MUMBAI'),('rgkulkarni1','f4445ec426f360f080b50cabc7a7016a','Ram KULKARNI','rgklt@hotmail.com','9879200015','L&T'),('Sham','9e786a2bd03c579979d8e71f08309817','Sham Kulkarni','shamk@hotmail.com','9879200066','iitd'),('Test','1bbd886460827015e5d605ed44252251','Test','test@test.test','9999999999','test'),('testuser123','f8aecd56598ad2140efdc974fded72cb','Test User','ameyaapte1@gmail.com','7588286543','COEP'),('vijaykmagapu','12c2d4df11712d4df8db779d910f80d9','Vijay Kumar Mahapu','vijayk.comagapu@gmail.com','9820042871','nil'),('vinay','78ffb54cea01b365797d0b883eba44fc','Vinay Kulkarni','vinay.vkulkarni@tcs.com','9225668804','TCS Research'),('vinayk','3f64b5b7551054ca4a495255eaf7343e','Vinay Vasant Kulkarni','vinay.vkulkarni@tcs.com','9225668804','Tata Consultancy Services ');
/*!40000 ALTER TABLE `userData` ENABLE KEYS */;

--
-- Dumping routines for database 'u686602787_pef'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-12 12:11:44
