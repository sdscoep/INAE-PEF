package org.coep.sds.conferenceregistration;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class InaugurationFragment extends Fragment {

    private static final Integer[] XMEN = {R.drawable.coep2, R.drawable.coep1, R.drawable.coep4};
    private static ViewPager mPager;
    private static int currentPage = 0;
    TextView aboutTextView;
    private ArrayList<Integer> XMENArray = new ArrayList<Integer>();
    public InaugurationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_inauguration, container, false);
        aboutTextView = (TextView) v.findViewById(R.id.about_text_view);
        fillData();
        for (int i = 0; i < XMEN.length; i++)
            XMENArray.add(XMEN[i]);

        mPager = (ViewPager) v.findViewById(R.id.pager);
        mPager.setAdapter(new PhotoAdapter(getContext(), XMENArray));
        CircleIndicator indicator = (CircleIndicator) v.findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == XMEN.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        /*Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);*/
        return v;
    }
    private void fillData(){

        Spanned aboutText = Html.fromHtml("<centre><h1>PEF Inauguration Function</h1></centre>"
                +"<b>Date and Time:</b> Friday July 27th at 4:30 PM\n" +
                "<br/><b>Venue:</b> Mini-Auditorium, Academic Complex, North Campus\n" +
                "             College of Engineering Pune, Shivaji Nagar, Pune- 411005\n"+
                "<h2>Program</h2>\n" +
                "<b><i>Inaugural Address:</i></b>\n" +
                "Dr. B N Suresh, President INAE, Chancellor IIST\n" +
                "Former Director VSSC, ISRO & Former Member Space Commission\n" +
                "\n<br/><b>Interactive Panel Discussion:</b>\n" +
                "<br/><b>\"Are Engineers of India ready for challenges of the next decade and beyond”</b>\n" +
                "<br/><b>Panelists:</b>\n" +
                "<ul><li>Prof. Bharatkumar Ahuja, Director College of Engineering</li>\n" +
                "<li>Dr. Sanjay Dhande, Former Director IIT Kanpur</li>\n" +
                "<li>Mr. Pravin Mehta, OS & DG Armament & Combat Engineering Systems, DRDO</li>\n" +
                "<li>Dr. Prahlada, Former Chief Controller R&D, DRDO</li>\n" +
                "<li>Dr. Anant Sardeshmukh, DG & Member of Executive Board MCCIA</li>\n" +
                "<li>Mr. Prakash Telang, Former MD Tata Motors</li>\n" +
                "<li>Dr. Harrick Vin, VP and Head Digitate, TCS</li></ul>"
                + "<centre><h2>Summary of the program </h2></centre>\n" +
                "The program was well attended. The audience of about 200, included engineers from Industry, Research Institutes and Academia. There were a large number of students from COEP and other colleges in Pune.\n" +
                "Dr. B N Suresh, President INAE gave the keynote address. He spoke about his vast experience in the Indian Space program. He inaugurated the Pune Engineering Forum and hoped that it will be a vibrant platform which will be useful to engineering community in Pune.\n" +
                "Mr M V Kotwal, introduced the eminent panelists. He spoke about INAE and it’s activities. He spoke how the Pune Engineering Forum will be useful to the engineering community here.\n" +
                "The other eminent Panelists narrated their experiences in the industry and spoke about their contribution to the national core sectors like Defence , Auto sector, IT etc.\n" +
                "In order to be ready for future, the speakers asked the young engineers to follow an integrated approach to become complete engineers and not just domain specialists. "
                +"They informed that exciting periods are ahead, but they will also be challenging since the traditional approach to learning & working will not be useful in future. A systems approach, integrated approach will be needed. \n" +
                "Things like Big-data, Data analytics, IOT , machine learning were already becoming the norm. Factories are getting more & more automated , with Robots, Driverless cars are being tried out, Drones are becoming smarter..  Hence all engineers need to keep up-to-date with the new developments, keep on learning new skills and explore new fields.\n" +
                "They told students to Innovate, take Patents. They spoke encouragingly on Start-ups.\n" +
                "The need for Industry- Academia interactions and making engineers ready for the industry was emphasized.\n" +
                "The program ended with Q&A from the gathering and vote of thanks by Dr Pradeep and Vinay Kulkarni. \n" +
                "\n" +
                "Vinay Kulkarni -FNAE & Secretary Pune Chapter INAE\n" +
                "M V Kotwal –    FNAE & Chairman Pune Chapter  INAE. "+ getString(R.string.inauguration)
        );
        aboutTextView.setText(aboutText);
    }
}
